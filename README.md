This Gulp process manages the development of PHP sites/apps/whatever. 

The default task cleans the 'dist/' directory and adds the .htaccess file
(if there is one) to the directory. Afterwards, it kicks off processing
PHP, Scss, and JS files. Gulp fires up a PHP server to host the files from 
the 'dist/' directory and uses BrowserSync as a proxy to handle automatic
updates to the browser.

-PHP files-
PHP files are simply copied over to the 'dist/' directory. BrowserSync
reloads the browser afterwards.

-Scss files-
Scss files are concatenated, processed into CSS, and then minified.
Sourcemaps are created for easier debugging. BrowserSync injects the 
minified CSS once the process is done.

-JS files-
JS related tasks are split into two categories: custom and vendor scripts.
Custom scripts are any JS files located in the 'src/assets/js' directory.
Any custom scripts are run through a linter (JSHint), concatenated, and 
uglified. BrowserSync reloads the page afterwards to inject the new JS.
Vendor scripts are located in the 'src/assets/js/vendor/' direcotry.
Vendor scripts are simply copied into the 'dist/assets/js/vendor' directory.

-Images-
Images are compressed and sent to the 'dist/assets/images' directory.
BrowserSync injects the images in on completion.

-Watch tasks-
All PHP, Scss, JS, and image files are watched.
