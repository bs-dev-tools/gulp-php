<?php
 
$app->hook('slim.before.dispatch', function () use ($app) {
	$view= $app->view();
    $view->setTemplatesDirectory('./views');

});
  
$app->hook('slim.after.dispatch', function () use ($app) {
	$view= $app->view();
     $view->setTemplatesDirectory('./views');

});

?>