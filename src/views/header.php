<?php include 'includes/meta.php' ?>
<body>
	<nav class="navbar navbar-default">
	  <div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#">LOGO/BRAND</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse">
		  <ul class="nav navbar-nav">
		   <?php 
				  $current =  Slim\Slim::getInstance()->router()->getCurrentRoute()->getName();
				  $routes = Slim\Slim::getInstance()->router()->getNamedRoutes(); 
				  foreach($routes as $route){ 
					$route_path = Slim\Slim::getInstance()->urlFor($route->getName());
					if ($current == $route->getName() && $current!="Offer" ){
					  echo "<li class='' id='active'><a href='{$route_path}' >{$route->getName()}</a></li>"; 
					}else{
					  echo "<li class=''><a href='{$route_path}' >{$route->getName()}</a></li>"; 
					}
				}
				?>
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>

	<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<!-- Content from modal.php goes here -->
			</div>
		</div>
	</div>
	<div class="container">
