$(document).ready(function() {
    $("#demoForm").validate({
        invalidHandler: function(event, validator) {
            console.log(validator);
            console.log(validator.errorList);
            $(validator.errorList).each(function(index, elem) {
                // Attach the bootstrap error validation state style
                $(elem.element).parents(".form-group").addClass("has-error");
                // console.log($(elem.element));
            });
        },
        submitHandler: function(form) {
            alert("Form was successful!");
            form.submit();
        }
    });
});
